# TeXTree

TeXTree is a tool for converting `.tex` code into HTML and json which can then
be displayed using a front-end system such as React or Angular. It is not meant
to generate standalone HTML.


## Usage

To execute the examples, create the `./build` directory, and execute
```bash
# in build/
python3 ../textree.py configure ../examples
ninja
```

"""
textree.py
"""
from pathlib import Path
import os, argparse, subprocess, json

import yaml, ninja

import textree.html

# Utilities

def _relative_path(x, y):
    """
    Calculate the relative path of x w.r.t. y even when x is not nested in y
    """
    return Path(os.path.relpath(x, y))
def _symlink(src, dst):
    """
    Create symlink from {src} to {dst}. If {src} is already a symlink then
    override the link. Otherwise do nothing. This does the minimal damage by
    not accidentally removing data.

    Return true if a link is established or already exists.
    """
    if src.is_symlink():
        src.unlink()
    if not src.exists():
        src.symlink_to(dst)
        return True
    else:
        return False



def configure_build_system(args):
    assert len(args.paths) == 1, "Only a single path can be provided"
    pathIn = Path(args.paths[0])
    with open(pathIn / 'textree.yaml', 'r') as f:
        projectConfig = yaml.safe_load(f)

    # Calculate the fundamental paths used in this project
    pathBuild = Path.cwd()
    pathCache = pathBuild / 'cache'
    pathOut = pathBuild / 'out'
    pathTextree = Path(__file__)
    pathLib = pathTextree.parent

    if args.verbose >= 1:
        print(f"Out path: {pathOut}")
        print(f"Build path: {pathBuild}")
        print(f"Cache path: {pathCache}")

    if not args.abspath:
        pathTextree = _relative_path(pathTextree, pathBuild)
        pathLib = _relative_path(pathLib, pathBuild)

    
    writer = ninja.Writer(open('build.ninja', 'w'))

    writer.comment('Generated rules call textree.py')
    writer.rule('pre',     f'python3 {pathTextree} pre $in $out')
    writer.rule('compile', f'python3 {pathTextree} compile $out')
    writer.rule('post',    f'python3 {pathTextree} post $in $out')
    writer.rule('meta',    f'python3 {pathTextree} meta $in $out')
    writer.rule('gather',  f'python3 {pathTextree} gather $in $out')

    writer.newline()
    for target in projectConfig['paths']:
        writer.comment(f'Target {target}')
        srcGlob = [x.relative_to(pathIn / target)
                   for x in (pathIn / target).glob('**/*.tex')]

        # Create single build rule for a target
        writer.build(str(pathCache / target / 'main.tex'), 'pre', \
                inputs=str(pathIn / target), \
                implicit_outputs = [ str(pathCache / target / x) for x in srcGlob if x != Path('main.tex')] )

        # The compilation depends on all .tex files in the source
        writer.build(str(pathCache / target / 'main.html'), 'compile', \
                inputs=[str(pathCache / target / x) for x in srcGlob] \
                      +[str(pathIn / target / x) for x in srcGlob])

        # Post compilation including HTML postprocessing and symbolic linking
        # all assets
        writer.build(str(pathOut / target / 'main.html'), 'post', \
                inputs=[str(pathCache / target / 'main.html')], \
                implicit_outputs=[ str(pathCache/target/'html_meta.json') ])

        # Metadata extraction
        writer.build(str(pathOut / target / 'meta.json'), 'meta', \
                inputs=[str(pathIn / target), str(pathCache / target / 'html_meta.json')], \
                implicit=[str(x) for x in (pathIn / target).glob('*.yaml')])

        # Phony rule to build an individual doc
        writer.build(target, 'phony', \
                inputs=[ str(pathOut / target / 'main.html'), str(pathOut / target / 'meta.json') ] )
    
    writer.build(str(pathOut / 'list.json'), 'gather', \
            inputs=[str(pathOut / target / 'meta.json') for target in projectConfig['paths']])

    
def stage_pre(args):
    pathIn = Path(args.paths[0])
    pathCache = Path(args.paths[1]).parent
    # Ensure output path exists
    pathCache.mkdir(exist_ok=True, parents=True)

    # Create symbolic links between input and cache paths
    paths = [x.relative_to(pathIn) for x in pathIn.glob('*')]

    if args.abspath:
        pathIn = pathIn.resolve()
    else:
        pathIn = _relative_path(pathIn, pathCache)

    for p in paths:
        if args.verbose >= 0:
            print(f"{p} -> {pathIn / p}")
        pathLink = pathCache / p
        _symlink(pathLink, pathIn / p)

def stage_compile(args):
    pathCache = Path(args.paths[0]).parent
    commands = [
        ['latexmk', '-pdf', '-halt-on-error'],
        ['lwarpmk', 'html'],
        ['lwarpmk', 'limages'],
        ['lwarpmk', 'html'],
    ]
    for command in commands:
        subprocess.run(command, cwd=pathCache, check=True)

    #(pathCache / 'main.html').touch()

def stage_post(args):
    pathCache = Path(args.paths[0]).parent
    pathOut = Path(args.paths[1]).parent

    meta = textree.html.process_html(pathCache / 'main.html', pathOut)

    with open(pathCache / 'html_meta.json', 'w') as f:
        json.dump(meta, f)


def stage_meta(args):
    pathIn = Path(args.paths[0])
    pathCacheMeta = Path(args.paths[1])
    pathOutMeta = Path(args.paths[2])

    with open(pathCacheMeta, 'r') as f:
        meta = json.load(f)

    # Attempt to read user specified metadata file
    pathInMeta = pathIn / 'meta.yaml'
    if pathInMeta.exists():
        with open(pathInMeta, 'r') as f:
            metaIn = yaml.safe_load(f)
        meta = {**meta, **metaIn}

    # Output the metadata file
    with open(pathOutMeta, 'w') as f:
        json.dump(meta, f)
        
    # Create symbolic links between cache and output paths
    pathCache = pathCacheMeta.parent
    pathOut = pathOutMeta.parent

    if args.abspath:
        pathIn_ = pathIn.resolve()
        pathCache = pathCache.resolve()
    else:
        pathIn_ = _relative_path(pathIn, pathOut)
        pathCache = _relative_path(pathCache, pathOut)

    # main-images symlinks
    _symlink(pathOut / 'main-images', pathCache / 'main-images')

    # symlink to any asset folder
    if (pathIn / 'assets').exists():
        _symlink(pathOut / 'assets', pathIn_ / 'assets')





def stage_gather(args):
    pathOutMeta = Path(args.paths[-1])
    pathOut = pathOutMeta.parent
    pathsMeta = [Path(x) for x in args.paths[:-1]]

    def extract(path):
        # Extract relative path
        relpath = path.parent.relative_to(pathOut)
        # Read meta
        with open(path, 'r') as f:
            meta = json.load(f)
        meta['path'] = str(relpath)
        return meta
    
    li = [extract(p) for p in pathsMeta]
    with open(pathOutMeta, 'w') as f:
        json.dump(li, f)


if __name__ == '__main__':

    # In Python3.7+ the order of keys in a dictionary is fixed
    executionDict = {
        'configure': configure_build_system,
        'pre': stage_pre,
        'compile': stage_compile,
        'post': stage_post,
        'meta': stage_meta,
        'gather': stage_gather,
    }
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', type=str, \
            help='Mode of execution ' + '|'.join(executionDict.keys()))
    parser.add_argument('paths', nargs='+',\
            help='Paths of operation')
    parser.add_argument('--abspath', action='store_true', \
            help='Store absolute paths in configuration files (otherwise store relative)')
    parser.add_argument('-v', '--verbose', type=int, default=0, \
            help='Verbosity (default 0)')
    args = parser.parse_args()

    if args.mode in executionDict:
        executionDict[args.mode](args)
    else:
        print(f"Unknown mode {args.mode}")

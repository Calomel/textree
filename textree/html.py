import os, sys, re, json
from pathlib import Path

import lxml
import lxml.html

def read_tree(path):
    tree = lxml.html.parse(str(path))
    
    elemHead = tree.find('head')
    elemBody = tree.find('body')\
        .find_class('textbody')[0]
    return elemHead, elemBody

# Head element
def extract_metadata(tree):
    # Find the description meta
    result = {
        'title': tree.find('title').text,
    }
    metas = tree.find_class('meta')
    for m in metas:
        if m.attrib['name'] == 'description':
            result['description'] = m.attrib['content']
    return result


# Body element

def _sanitise_link(name, level):
    name = re.sub('[^A-Za-z\d\.-]', '', name).lower()
    return level + '-' + name

def extract_toc(tree):
    toc = tree.find_class('toc')
    if len(toc) == 0:
        return None
    toc, = toc
    
    # Parse the toc to create a autosec-label map
    linkObjects = toc.findall('.//a') # Find all a-elements corresponding to links
    linkMap = dict()
    for obj in linkObjects:
        idAutosec = obj.attrib['href'].split('#')[-1]
        
        objSpan = obj.find('span')
        if objSpan is not None:
            name = objSpan.text
        else:
            name = obj.text
        level = obj.classes.pop()[len('toc'):]
        idMapped = _sanitise_link(name, level)
        obj.classes.add('toc-' + level)
        obj.attrib['href'] = '#' + idMapped
        linkMap[idAutosec] = idMapped
    return toc, linkMap

def relabel_body(tree, tocExtraction):
    
    # Remove the titlepage since it is handled separately in the front-end
    objTitlepage = tree.find_class('titlepage')
    if objTitlepage:
        tree.remove(objTitlepage[0])
    
    if tocExtraction:
        toc, linkMap = tocExtraction
        # Remove everything before the toc
        for obj in tree.iterchildren():
            tree.remove(obj)
            if obj == toc:
                break
        
        # Re-map the id
        for obj in tree.iterdescendants():
            objId = obj.attrib.get('id', '')
            if linkMap and objId in linkMap:
                obj.attrib['id'] = linkMap[objId]
    
    return tree

def process_html(pathIn, pathOut):
    """
    Read the html file {pathIn} and output to {pathOut}/toc.html and
    {pathOut}/body.html.

    Returns the metadata extracted from HTML header
    """
    pathIn = Path(pathIn)
    pathOut = Path(pathOut)

    elemHead, elemBody = read_tree(pathIn)
    tocExtraction = extract_toc(elemBody)
    elemBody = relabel_body(elemBody, tocExtraction)

    meta = extract_metadata(elemHead)

    if tocExtraction is not None:
        toc, _ = tocExtraction
        with open(pathOut / 'toc.html', 'wb') as f:
            f.write(lxml.html.tostring(toc))

    with open(pathOut / 'main.html', 'wb') as f:
        f.write(lxml.html.tostring(elemBody))

    return meta

